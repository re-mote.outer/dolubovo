function scrollAnimateToBlock(event,block,offset){//block
  event.preventDefault();
  let dest = 0;
  let top = offset || 0;
  if (block){
    dest = $(block).offset().top + top - 60;
  }
  $('html, body').animate({scrollTop: dest}, 700);
}
